#include <cstdlib>
#include <cmath>
#include <lv2.h>
#include "entry.h"

#define PLUGIN_URI "http://hiogawa.net/plugins/some_looper_metro"

#define PLUGIN_PORT_LIST(V) \
  V(Output) \
  V(SyncOutput) \
  V(CtrlBpm) \
  V(CtrlSignalOnXBeats) \
  V(CtrlClickOnXBeats)

enum PluginPort {
#define DECLARE_PORT_ENUM(Name) k##Name,
  PLUGIN_PORT_LIST(DECLARE_PORT_ENUM)
#undef DECLARE_PORT_ENUM
  PluginPortEnd
};

#define CLICK_WAVE_HZ (440 * 2)
#define CLICK_WAVE_AMP 0.5
#define CLICK_ATTACK_SEC 0.005
#define CLICK_DECAY_SEC 0.075
#define PI (acos(0) * 2)

enum ClickState {
  ATTACK, // Envelope rising
  DECAY,  // Envelope lowering
  OFF     // Silent
};

class SomeLooperMetro {
public:
  SomeLooperMetro(double);
  ~SomeLooperMetro();
  void ConnectPort(uint32_t, void*);
  void Run(uint32_t);

private:
  #define DECLARE_PORT(Name) float* port_##Name;
    PLUGIN_PORT_LIST(DECLARE_PORT)
  #undef DECLARE_PORT

  double sample_rate_;
  float* click_wave_buffer_;
  uint32_t click_wave_buffer_size_;
  uint32_t click_wave_buffer_offset_;

  uint32_t click_attack_size_;
  uint32_t click_decay_size_;
  uint32_t click_elapsed_offset_;
  ClickState click_state_;

  uint32_t sync_signal_elapsed_offset_;
};

SomeLooperMetro::SomeLooperMetro(double sample_rate) {
  sample_rate_ = sample_rate;
  click_wave_buffer_size_ = static_cast<uint32_t>(sample_rate_ / CLICK_WAVE_HZ);
  click_wave_buffer_ = reinterpret_cast<float*>(calloc(click_wave_buffer_size_, sizeof(float)));
  for (uint32_t i = 0; i < click_wave_buffer_size_; i++) {
    click_wave_buffer_[i] = static_cast<float>(sin((2 * PI) * i / click_wave_buffer_size_) * CLICK_WAVE_AMP);
  }
  click_wave_buffer_offset_ = 0;

  click_attack_size_ = static_cast<uint32_t>(CLICK_ATTACK_SEC * sample_rate_);
  click_decay_size_ = static_cast<uint32_t>(CLICK_DECAY_SEC * sample_rate_);
  click_state_ = OFF;
  click_elapsed_offset_ = 0;
  sync_signal_elapsed_offset_ = 0;
}

SomeLooperMetro::~SomeLooperMetro() {
  free(click_wave_buffer_);
}

void SomeLooperMetro::ConnectPort(uint32_t port, void* data_location) {
  switch (port) {
#define CASE(Name) case k##Name: \
    port_##Name = reinterpret_cast<float*>(data_location); \
    break;
    PLUGIN_PORT_LIST(CASE)
#undef CASE
  }
}

void SomeLooperMetro::Run(uint32_t num_samples) {
  float num_frames_per_beat = (60 / *port_CtrlBpm) * static_cast<float>(sample_rate_);
  uint32_t num_frames_per_signal = static_cast<uint32_t>(
    num_frames_per_beat * *port_CtrlSignalOnXBeats);
  uint32_t num_frames_per_click = static_cast<uint32_t>(
    num_frames_per_beat * *port_CtrlClickOnXBeats);

  for (uint32_t pos = 0; pos < num_samples; pos++) {
    float* out_sample_ptr = &port_Output[pos];
    float* sync_out_sample_ptr = &port_SyncOutput[pos];

    // zero out output ports
    *out_sample_ptr = *sync_out_sample_ptr = 0;

    // check if sync signal should be sent
    sync_signal_elapsed_offset_ %= num_frames_per_signal;
    if (sync_signal_elapsed_offset_ == 0) {
      *sync_out_sample_ptr = 1.0;
    }

    // check if click should start
    click_elapsed_offset_ %= num_frames_per_click;
    if (click_elapsed_offset_ == 0) {
      click_state_ = ATTACK;
      click_elapsed_offset_ = 0;
    }

    // Write click sound sample
    float coeff = 0;
    float x = static_cast<float>(click_elapsed_offset_),
          y = static_cast<float>(click_attack_size_),
          z = static_cast<float>(click_decay_size_);
    switch (click_state_) {
      case ATTACK:
        coeff = x / y;
        if (click_elapsed_offset_ >= click_attack_size_) {
          click_state_ = DECAY;
        }
        break;
      case DECAY:
        coeff = 1 - (x - y) / z;
        if (click_elapsed_offset_ - click_attack_size_ >= click_decay_size_) {
          click_state_ = OFF;
        }
        break;
      case OFF:
        coeff = 0;
        break;
    }
    *out_sample_ptr = click_wave_buffer_[click_wave_buffer_offset_] * coeff;

    // Progress internal frame counting
    click_wave_buffer_offset_++;
    click_wave_buffer_offset_ %= click_wave_buffer_size_;

    click_elapsed_offset_++;
    sync_signal_elapsed_offset_++;
  }
}


// LV2 interface

namespace {

LV2_Handle Instantiate(const LV2_Descriptor*, double sample_rate,
                       const char*, const LV2_Feature* const*) {
  return reinterpret_cast<LV2_Handle>(new SomeLooperMetro(sample_rate));
}

void ConnectPort(LV2_Handle handle, uint32_t port, void* data_location) {
  return reinterpret_cast<SomeLooperMetro*>(handle)->ConnectPort(port, data_location);
}

void Run(LV2_Handle handle, uint32_t num_samples) {
  reinterpret_cast<SomeLooperMetro*>(handle)->Run(num_samples);
}

void Activate(LV2_Handle) {}
void Deactivate(LV2_Handle) {}
void Cleanup(LV2_Handle handle) {
  delete reinterpret_cast<SomeLooperMetro*>(handle);
}

const void* ExtensionData(const char*) { return nullptr; }

static LV2_Descriptor descriptor = {
  PLUGIN_URI,
  Instantiate,
  ConnectPort,
  Activate,
  Run,
  Deactivate,
  Cleanup,
  ExtensionData,
};

} // namespace

namespace entry {
  LV2_Descriptor* SomeLooperMetroDescriptor() { return &descriptor; }
}
