#include <cstdlib> // calloc, free
#include <algorithm> // max
#include <lv2.h>
#include "entry.h"

#define PLUGIN_URI "http://hiogawa.net/plugins/some_looper"
#define LOOP_BUFFER_LENGTH_IN_SECONDS 5 * 60

// Macro trick I saw in v8 code base
#define PLUGIN_PORT_LIST(V) \
  /* == Audio ports == */ \
  V(Input) \
  V(Output) \
  V(SyncInput) \
  V(SyncOutput) \
  /* == Engine interface == */ \
  /* -- Static parameters -- */ \
  V(CtrlSync) \
  V(CtrlMonitor) \
  V(CtrlRecordMode) \
  /* -- Trigger control inputs -- */ \
  V(CtrlProgress_Start) V(CtrlProgress_Stop) \
  V(CtrlRecord_Start) V(CtrlRecord_Stop) \
  V(CtrlPlayback_Start) V(CtrlPlayback_Stop) \
  V(CtrlBackToBeginning_Start) /* aka trigger */ \
  V(CtrlClearLoop_Start) \
  /* -- Engine state feedbacks (as control output) -- */ \
  V(StateRecording) \
  V(StatePlaybacking) \
  V(StateOverdubbing) /* aka copying previous loop while progressing */ \
  V(StateProgressing) /* TODO: won't be different from CtrlProgress */ \
  V(StateWrapping) \
  V(StateWaitingSync) /* engine's state change is waiting for sync signal to move on */ \
  V(StateWaitingUser) /* engine's state change has finished and is waiting for user's control input */ \
  V(StateLoopLength) \
  V(StateLoopPosition)

enum PluginPort {
#define DECLARE_PORT_ENUM(Name) k##Name,
  PLUGIN_PORT_LIST(DECLARE_PORT_ENUM)
#undef DECLARE_PORT_ENUM
  PluginPortEnd
};

enum RecordMode {
  RECORDMODE_NORMAL = 0,
  RECORDMODE_OVERDUB = 1,
  RECORDMODE_SUBSTITUTE = 2
};

// Engine's internal 2-value states
enum StateBits : uint32_t {
  RECORDING =    1 << 0,
  PLAYBACKING =  1 << 1,
  OVERDUBBING =  1 << 2,
  PROGRESSING =  1 << 3,
  WRAPPING =     1 << 4,
  WAITING_SYNC = 1 << 5,
  WAITING_USER = 1 << 6
};

class SomeLooper {
public:
  SomeLooper(double);
  ~SomeLooper();
  void ConnectPort(uint32_t, void*);
  void Run(uint32_t);

private:
  bool Invariant();
  void ClearLoop();
  void SolveSyncSignal();
  void SolveStatesInterRelation();

  // Lv2 instance params
  double sample_rate;

  // Lv2 ports
  #define DECLARE_PORT(Name) float* port_##Name;
    PLUGIN_PORT_LIST(DECLARE_PORT)
  #undef DECLARE_PORT

  // pre-allocated audio buffer for loop frames
  float* buffer;
  uint32_t buffer_size;

  // recorded loop size (number of frames)
  uint32_t loop_size;

  // current loop position to playback (or recording)
  uint32_t loop_offset;

  // current state (as a combination of StateBits)
  uint32_t state;

  // next state to transition after waiting for sync signal
  uint32_t next_state_set;
  uint32_t next_state_unset;
};

SomeLooper::SomeLooper(double _sample_rate) : sample_rate(_sample_rate) {
  buffer_size = static_cast<uint32_t>(sample_rate * LOOP_BUFFER_LENGTH_IN_SECONDS);
  buffer = reinterpret_cast<float*>(calloc(buffer_size, sizeof(float)));
  loop_size = 1;
  loop_offset = 0;
  state = WAITING_USER | PROGRESSING;
  next_state_set = 0;
  next_state_unset = 0;
}

SomeLooper::~SomeLooper() {
  free(buffer);
}

bool SomeLooper::Invariant() {
// TODO: lv2 logging
// #define INVARIANT_LIST(V)
  // validate current engine's state e.g.
  // - 0 <= loop_offset <= loop_size <= buffer_size
  // - !((state & WAITING_SYNC) && (state & WAITING_SYNC))
  // - (state & WAITING_SYNC) || (state & WAITING_SYNC)
  // - if (state & WRAPPING) { (state & PROGRESSING) }
  return true;
}

void SomeLooper::ClearLoop() {
  for (uint32_t i = 0; i < loop_size; i++) {
    buffer[i] = 0;
  }
  loop_offset = 0;
  loop_size = 1;
}

void SomeLooper::ConnectPort(uint32_t port, void* data) {
  switch (port) {
#define CASE(Name) case k##Name: \
    port_##Name = reinterpret_cast<float*>(data); \
    break;
    PLUGIN_PORT_LIST(CASE)
#undef CASE
  }
}

void SomeLooper::SolveSyncSignal() {
  state = ((state | next_state_set) & ~next_state_unset);
  state = (state | WAITING_USER) & ~WAITING_SYNC;
  next_state_set = 0;
  next_state_unset = 0;
  SolveStatesInterRelation();
}

void SomeLooper::SolveStatesInterRelation() {
  if (state & RECORDING) {
    switch(static_cast<int>(*port_CtrlRecordMode)) {
      case RECORDMODE_NORMAL:
        state = ((state & ~PLAYBACKING) & ~OVERDUBBING) & ~WRAPPING;
        break;
      case RECORDMODE_OVERDUB:
        state = (state | PLAYBACKING) | OVERDUBBING;
        break;
      case RECORDMODE_SUBSTITUTE:
        state = (state | PLAYBACKING) & ~OVERDUBBING;
        break;
    }
  } else {
    state = (state | WRAPPING) | OVERDUBBING;
  }
}

void SomeLooper::Run(uint32_t n_samples) {
  // 1. State transition based on given control inputs and current state
  if (state & WAITING_USER) {
    if (*port_CtrlSync >= 1.0f) {
      if (*port_CtrlRecord_Start >= 1.0f && !(state & RECORDING)) {
        state = (state | WAITING_SYNC) & ~WAITING_USER;
        next_state_set = RECORDING;
      }
      if (*port_CtrlRecord_Stop >= 1.0f && (state & RECORDING)) {
        state = (state | WAITING_SYNC) & ~WAITING_USER;
        next_state_unset = RECORDING;
      }

      if (*port_CtrlPlayback_Start >= 1.0f && !(state & PLAYBACKING)) {
        state = (state | WAITING_SYNC) & ~WAITING_USER;
        next_state_set = PLAYBACKING;
      }
      if (*port_CtrlPlayback_Stop >= 1.0f && (state & PLAYBACKING)) {
        state = (state | WAITING_SYNC) & ~WAITING_USER;
        next_state_unset = PLAYBACKING;
      }

    } else {
      if (*port_CtrlRecord_Start >= 1.0f && !(state & RECORDING)) {
        state |= RECORDING;
      }
      if (*port_CtrlRecord_Stop >= 1.0f && (state & RECORDING)) {
        state &= ~RECORDING;
      }

      if (*port_CtrlPlayback_Start >= 1.0f && !(state & PLAYBACKING)) {
        state |= PLAYBACKING;
      }
      if (*port_CtrlPlayback_Stop >= 1.0f && (state & PLAYBACKING)) {
        state &= ~PLAYBACKING;
      }

      SolveStatesInterRelation();
    }

    // TODO: Progress can be synced ?
    if (*port_CtrlProgress_Start >= 1.0f && !(state & PROGRESSING)) {
      state |= PROGRESSING;
    }
    if (*port_CtrlProgress_Stop >= 1.0f && (state & PROGRESSING)) {
      state &= ~PROGRESSING;
    }
  }

  // TODO: BackToBeginning can be synced ?
  if (*port_CtrlBackToBeginning_Start >= 1.0f) {
    loop_offset = 0;
    state = (state | WAITING_USER) & ~WAITING_SYNC;
  }

  if (*port_CtrlClearLoop_Start >= 1.0f) {
    ClearLoop();
    state = (state | WAITING_USER) & ~WAITING_SYNC;
  }


  // 2. Process samples (possiblly with state transition because of sync)
  for (uint32_t pos = 0; pos < n_samples; pos++) {
    float in_sample = port_Input[pos];
    float* out_sample_ptr = &port_Output[pos];
    float* sync_out_sample_ptr = &port_SyncOutput[pos];
    float* loop_sample_ptr = &buffer[loop_offset];
    float loop_sample_prev = *loop_sample_ptr; // Cache value since sample buffer is overwritten in place

    // 2.0 zero out new samples
    *out_sample_ptr = *sync_out_sample_ptr = 0;

    // 2.1. Process sync signal
    if (state & WAITING_SYNC && *port_CtrlSync >= 1.0f && port_SyncInput[pos] >= 1.0f) {
      SolveSyncSignal();
    }

    // 2.2. Generates audio data
    if (*port_CtrlMonitor >= 1.0f) {
      *out_sample_ptr += in_sample;
    }
    if (state & PROGRESSING) {
      if (state & PLAYBACKING) {
        *out_sample_ptr += loop_sample_prev;
      }

      // zero out loop sample
      *loop_sample_ptr = 0;

      if (state & RECORDING) {
        *loop_sample_ptr += in_sample;
      }
      if (state & OVERDUBBING) {
        *loop_sample_ptr += loop_sample_prev;
      }
    }

    // 2.3. progress loop offset and output sync signal when it's loop boundary
    if (state & PROGRESSING) {
      loop_offset++;
      if (state & WRAPPING) {
        loop_offset %= loop_size;
        if (loop_offset == 0 && loop_offset > 1) {
          *sync_out_sample_ptr = 1.0;
        }
      } else {
        loop_size = std::max(loop_offset + 1, loop_size);
      }
    }
  }

  // 3. setup feedback states
  *port_StateRecording        = (state & RECORDING) ? 1.0 : 0.0;
  *port_StatePlaybacking      = (state & PLAYBACKING) ? 1.0 : 0.0;
  *port_StateOverdubbing      = (state & OVERDUBBING) ? 1.0 : 0.0;
  *port_StateProgressing      = (state & PROGRESSING) ? 1.0 : 0.0;
  *port_StateWrapping         = (state & WRAPPING) ? 1.0 : 0.0;
  *port_StateWaitingSync      = (state & WAITING_SYNC) ? 1.0 : 0.0;
  *port_StateWaitingUser      = (state & WAITING_USER) ? 1.0 : 0.0;
  *port_StateLoopLength       = loop_size == 1 ? 0 : loop_size / static_cast<float>(sample_rate);
  *port_StateLoopPosition     = loop_offset / static_cast<float>(sample_rate);
}


// LV2 interface

namespace {

static LV2_Handle Instantiate(const LV2_Descriptor*, double sample_rate,
                              const char*, const LV2_Feature* const*) {
  return reinterpret_cast<LV2_Handle>(new SomeLooper(sample_rate));
}

static void ConnectPort(LV2_Handle handle, uint32_t port, void* data) {
  reinterpret_cast<SomeLooper*>(handle)->ConnectPort(port, data);
}

static void Activate(LV2_Handle) {}

static void Run(LV2_Handle handle, uint32_t n_samples) {
  reinterpret_cast<SomeLooper*>(handle)->Run(n_samples);
}

static void Deactivate(LV2_Handle) {}

static void Cleanup(LV2_Handle handle) {
  delete reinterpret_cast<SomeLooper*>(handle);
}

static const void* ExtensionData(const char*) {
  return nullptr;
}

static LV2_Descriptor descriptor = {
  PLUGIN_URI,
  Instantiate,
  ConnectPort,
  Activate,
  Run,
  Deactivate,
  Cleanup,
  ExtensionData,
};

} // namespace

namespace entry {
  LV2_Descriptor* SomeLooperDescriptor() { return &descriptor; }
}
