cmake_minimum_required(VERSION 2.8.12)

project(some_looper LANGUAGES CXX)

include(FindPkgConfig)
pkg_check_modules(LV2 REQUIRED lv2)

set(CMAKE_CXX_COMPILER clang++)
set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_FLAGS
  "-Weverything \
   -Wno-padded \
   -Wno-c++98-compat \
   ${CMAKE_CXX_FLAGS}")

add_library(some_looper_entry SHARED some_looper.cc some_looper_metro.cc entry.cc)
target_include_directories(some_looper_entry PRIVATE LV2_INCLUDE_DIRS)

set(PLUGIN_DIR lib/lv2/some_looper.lv2)
install(TARGETS some_looper_entry
  DESTINATION ${PLUGIN_DIR})
install(FILES manifest.ttl some_looper.ttl some_looper_metro.ttl
  DESTINATION ${PLUGIN_DIR})
