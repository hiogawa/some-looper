# Development

```
$ cmake -B build -DCMAKE_INSTALL_PREFIX=$PWD/build/install
$ cmake --build build
$ cmake --install build
$ LV2_PATH=/usr/lib/lv2:$PWD/build/install/lib/lv2 jalv.gtk http://hiogawa.net/plugins/some_looper
```

# Note

TODO:

- [x] monitor; record; playback loop;
- [x] sync (aka quantize)
- [x] sync signal only metronome plugin;
- [x] make it work on carla (currently if this plugin is loaed, carla dies.)
- [x] prepare licence (GPL)
- [ ] name this plugin well
- [?] incorporate metronome functionality into main looper plugin
- [ ] prepare separate "sync" mode for normal recording, overdub, playback ?
- [ ] "formally" define this ever complicated state transition system
- [?] less clicky recording start/stop
- [ ] stereo recording
- [ ] support midi loop
- [ ] ui plugin and implement control as parameter patch set/get
- [?] pprops:triger not working on host ? (it worked on Carla. maybe not in Jalv)
- [ ] validate ttl with sord_validate on build time
- [ ] better compiler setup in cmake
- [?] threshold start (don't needed ?)
- [ ] better README


Some scratch:

- Difference from SooperLooper
  - no insert
  - no sync playback
  - rate/pictch change, reversing
  - sync only by loop length
  - ..
- Loop functionality will be closer to Giada (fixed loop length)
