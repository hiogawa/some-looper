#include <lv2.h>

namespace entry {
  LV2_Descriptor* SomeLooperDescriptor();
  LV2_Descriptor* SomeLooperMetroDescriptor();
}
