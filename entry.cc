#include <lv2.h>
#include "entry.h"

const LV2_Descriptor* lv2_descriptor(uint32_t index) {
  switch (index) {
    case 0: return entry::SomeLooperDescriptor();
    case 1: return entry::SomeLooperMetroDescriptor();
    default: return nullptr;
  }
}
